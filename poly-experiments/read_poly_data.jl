using DataFrames
using Dates
using CSV

function read_poly_data(file; logging=false)::DataFrame
    if logging
        @info "Loading data from $file"
    end
    df = CSV.File(
        file, 
        dateformat = "yyyy-mm-dd HH:MM:SS",
        types = [
            DateTime, # t (Time (s))
            Float32, # v (Volume)
            Float32, # vw (Volume weighted)
            Float32, # o (Open)
            Float32, # c (Close)
            Float32, # h (High)
            Float32, # l (Low)
            Float32 # n
            ]
    ) |> DataFrame
    if logging
        @info "Loaded $(nrow(df)) rows of stock data from $file"
    end
    df
end

function read_poly_data(file1, file2, files...; logging=false, on=:t)::DataFrame
    data1 = read_poly_data(file1; logging=logging)
    data2 = read_poly_data(file2; logging=logging)
    rest_data = [read_poly_data(file; logging=logging) for file in files]
    if logging
        @info "Joining data for $(length(rest_data) + 2) files"
    end
    data = innerjoin(data1, data2, rest_data...; on=on, makeunique=true)
    if logging
        @info "Successfully joined data into $(nrow(data)) rows of processed tick data"
    end
    data
end