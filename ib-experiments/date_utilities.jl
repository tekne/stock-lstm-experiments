using Dates

const YEAR_MS = 3154000000
const MONTH_MS = 259200000
const HOUR_MS = 3600000

function to_clock_sin(date::DateTime, period) :: Float32
    unix_time :: Float64 = Dates.value(date)
    sin(2 * pi * unix_time / period)
end

function to_clock_cos(date::DateTime, period) :: Float32
    unix_time :: Float64 = Dates.value(date)
    cos(2 * pi * unix_time / period)
end

function linear_hour(date::DateTime) :: Float32
    Dates.hour(date) / 24.0 + Dates.minute(date) / 1440.0
end

function to_clock(date::DateTime, period) :: Tuple{Float32, Float32}
    (to_clock_sin(date, period), to_clock_cos(date, period))
end