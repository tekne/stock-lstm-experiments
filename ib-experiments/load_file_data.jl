using DataFrames
using DataFrames: ByRow
using CSV
using Lathe.preprocess: StandardScalar
include("date_utilities.jl")

function row_differences!(vector)
    l = length(vector)
    for i in 0:(l - 2)
        vector[l - i] -= vector[l - i - 1]
    end
    vector
end

function row_differences(vector)
    row_differences!(copy(vector))
end

"Load stock data from an IB formatted OHLC CSV file"
function load_stock_data(file; logging=true)
    if logging
        @info "Loading stock data from $file"
    end
    data = CSV.File(file; dateformat="yyyy-mm-dd HH:MM:SS") |> DataFrame
    if logging
        @info "Loaded $(nrow(data)) points of ticker data from $file, processing"
    end
    #sort!(data, :date)
    x_data = select(
        data,
        :date => ByRow(t -> to_clock_sin(t, YEAR_MS)) => :year_sin,
        :date => ByRow(t -> to_clock_cos(t, YEAR_MS)) => :year_cos,
        :date => ByRow(t -> to_clock_sin(t, MONTH_MS)) => :month_sin,
        :date => ByRow(t -> to_clock_cos(t, MONTH_MS)) => :month_cos,
        :date => ByRow(t -> to_clock_sin(t, HOUR_MS)) => :hour_sin,
        :date => ByRow(t -> to_clock_cos(t, HOUR_MS)) => :hour_cos,
        :date => ByRow(linear_hour) => :day_time,
        :date, 
        :open => ByRow(Float32) => :open, 
        :high => ByRow(Float32) => :high, 
        :low => ByRow(Float32) => :low, 
        :close => ByRow(Float32) => :close
    )
    transform!(
        x_data,
        :open => row_differences => :open_diff, 
        :high => row_differences => :high_diff, 
        :low => row_differences => :low_diff, 
        :close => row_differences => :close_diff
    )
    transform!(
        x_data,
        :open_diff => StandardScalar => :open_diff, 
        :high_diff => StandardScalar => :high_diff, 
        :low_diff => StandardScalar => :low_diff, 
        :close_diff => StandardScalar => :close_diff
    )
    transform!(
        x_data,
        :open => StandardScalar => :open,
        :high => StandardScalar => :high,
        :low => StandardScalar => :low,
        :close => StandardScalar => :close
    )
    y_data = select!(
        data,
        :date,
        :high => row_differences => :high_diff,
        :low => row_differences => :low_diff
    )
    transform!(
        y_data,
        :high_diff => StandardScalar => :high_diff,
        :low_diff => StandardScalar => :low_diff
    )
    transform!(
        y_data,
        :high_diff => ByRow(Float32) => :high_diff,
        :low_diff => ByRow(Float32) => :low_diff
    )
    if logging
        @info "Processed data from $file"
    end
    return (x_data[2:(nrow(data) - 1), :], y_data[3:end, :])
end

function load_stock_data(file1, file2, files...; logging=true)
    (x_data1, y_data1) = load_stock_data(file1; logging=logging)
    (x_data2, y_data2) = load_stock_data(file2; logging=logging)
    rest_data = [load_stock_data(file) for file in files]
    rest_x_data = [data[1] for data in rest_data]
    rest_y_data = [data[2] for data in rest_data]
    if logging
        @info "Joining data for $(length(rest_data) + 2) files"
    end
    x_data = select!(innerjoin(x_data1, x_data2, rest_x_data...; on=:date, makeunique=true), Not(:date))
    y_data = select!(innerjoin(y_data1, y_data2, rest_y_data...; on=:date, makeunique=true), Not(:date))
    if logging
        @info "Successfully joined data into $(nrow(x_data)) rows of processed tick data"
    end
    (x_data, y_data)
end